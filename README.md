Swedish version of the Norwegian official standard for dvorak layout (svdvorak)
with addition of printed qwerty keys for multi-layout purposes. Vector template
for printing via https://wasdkeyboards.com/. Font to be used has not been
decided.